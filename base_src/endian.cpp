#include <rtdl/endian.hpp>

namespace RTDL {

template<>
uint16_t convert<uint16_t>(uint16_t val, Endian from, Endian to) {
    if(from == to)
        return val;
    return __builtin_bswap16(val);
}
template<>
uint32_t convert<uint32_t>(uint32_t val, Endian from, Endian to) {
    if(from == to)
        return val;
    return __builtin_bswap32(val);
}
template<>
uint64_t convert<uint64_t>(uint64_t val, Endian from, Endian to) {
    if(from == to)
        return val;
    return __builtin_bswap64(val);
}
}
