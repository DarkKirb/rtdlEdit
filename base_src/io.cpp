#include <rtdl/io.hpp>
#include <algorithm>
#include <stdexcept>

namespace RTDL {
IOBase::IOBase(std::string fname): _io((IOBase*)nullptr) {
    _file = new std::fstream;
    _file->open(fname.c_str(), std::ios::ate | std::ios::binary | std::ios::in | std::ios::out);
    if(!_file->is_open())
        throw std::runtime_error("File could not be opened");
    _io_pos = 0;
    offset = 0;
    size = _file->tellg();
    _file->seekg(0);
}
IOBase::IOBase(shared_ptr<IOBase> file): _io(file) {
    _file = nullptr;
    _io_pos = 0;
    offset = file->tell();
    file->seek(0, std::ios_base::end);
    size = file->tell();
    file->seek(offset);
}
IOBase::IOBase(shared_ptr<IOBase> file, uint64_t size): IOBase(file) {
    this->size = size;
}

IOBase::~IOBase() {
    if(_file)
        delete _file;
}

uint64_t IOBase::raw_read(void *dest, uint64_t size) {
    if(_io) {
        _io->seek(_io_pos + offset);
        auto diff = std::min(_io->read(dest, size), size - _io_pos);
        _io_pos += diff;
        return diff;
    }
    _file->read((char *)dest, size);
    return _file->gcount();
}
uint64_t IOBase::raw_write(const void *src, uint64_t size) {
    if(_io) {
        _io->seek(_io_pos + offset);
        _io_pos += size;
        return _io->write(src, size);
    }
    _file->write((const char *)src, size);
    return size;
}
uint64_t IOBase::raw_tell() {
    if(_io)
        return _io_pos;
    return _file->tellg();
}
void IOBase::raw_seek(int64_t pos, std::ios_base::seekdir dir) {
    if(_io) {
        if(dir == std::ios_base::beg)
            _io_pos = pos;
        else if(dir == std::ios_base::end)
            _io_pos = size - pos;
        else
            _io_pos += pos;
        return;
    }
    _file->seekg(pos, dir);
}
}
