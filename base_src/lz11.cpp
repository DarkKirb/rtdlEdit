#include <rtdl/lz11.hpp>
#include <stdexcept>
#include <algorithm>
#include <string.h>
#include <iostream>
namespace RTDL {
LZ11::LZ11(std::string fname): IOBase(fname), pos(0), modified(false) {
    decompress();
}
LZ11::LZ11(shared_ptr<IOBase> file): IOBase(file), pos(0), modified(false) {
    decompress();
}
LZ11::~LZ11() {
    compress();
}

void LZ11::decompress() {
    uint32_t size;
    raw_read(&size, 4);
    size = convert<Endian::Little>(size);
    if((size&0xFF) != 0x11)
        throw std::runtime_error("Invalid LZ11 header");
    size >>= 8;
    if(!size) {
        raw_read(&size, 4);
        size = convert<Endian::Little>(size);
    }
    dec_data.reserve(size);
    uint8_t byte;
    while(dec_data.size() < size) {
        if(!raw_read(&byte, 1))
            break;
        for(int bit_no=7; bit_no >= 0; bit_no--) {
            if(dec_data.size() == size)
                break;
            bool bit = (bool)((byte >> bit_no)&1);
            if(!bit) {
                uint8_t data;
                raw_read(&data, 1);
                dec_data.push_back(data);
            } else {
                uint8_t lenmsb, lsb;
                raw_read(&lenmsb, 1);
                raw_read(&lsb, 1);
                uint32_t length = lenmsb >> 4;
                uint32_t disp = ((lenmsb & 15) << 8) + lsb;
                if(length > 1) {
                    length++;
                } else if(length == 0) {
                    length = (lenmsb & 15) << 4;
                    length += lsb >> 4;
                    length += 0x11;
                    uint8_t msb;
                    raw_read(&msb, 1);
                    disp = ((lsb & 15) << 8) + msb;
                } else {
                    length = (lenmsb & 15) << 12;
                    length += lsb << 4;
                    uint8_t byte1, byte2;
                    raw_read(&byte1, 1);
                    raw_read(&byte2, 1);
                    length += byte1 >> 4;
                    disp = ((byte1 & 15) << 8) + byte2;
                    length += 0x111;
                }
                uint32_t start = dec_data.size() - disp - 1;
                for(uint32_t i=0; i < length; i++) {
                    dec_data.push_back(dec_data[start + i]);
                }
            }
        }
    }
}

void LZ11::compress() {
    if(!modified)
        return;
    raw_seek(0, std::ios_base::beg);
    uint32_t header = 0x11;
    if(dec_data.size() < (1 << 24)) {
        header += dec_data.size() << 8;
        header = convert<Endian::Little>(header);
        raw_write(&header, 4);
    } else {
        header = convert<Endian::Little>(header);
        raw_write(&header, 4);
        uint32_t size = convert<Endian::Little>((uint32_t)dec_data.size());
        raw_write(&size, 4);
    }
    uint32_t off = 0;
    uint8_t byte = 0;
    int index = 7;
    std::vector<char> cmpbuf;
    while(off < dec_data.size()) {
        auto longest_pos = dec_data.begin() + off;
        uint32_t longest_len = 0;
        for(int length = 0; length < std::min(0x10110ul, dec_data.size() - off); length++) {
            uint32_t start = 0;
            if(off >= 0x1000)
                start = off - 0x1000;
            auto cur_pos = std::search(dec_data.begin() + start, dec_data.end(), dec_data.begin() + off, dec_data.begin() + off + length);
            if(cur_pos == dec_data.begin() + off)
                break;
            if(std::distance(dec_data.begin(), cur_pos) >= off)
                break;
            longest_pos = cur_pos;
            longest_len = length;
        }
        if(longest_len < 3) {
            std::cerr << "Writing RAW: " << (int)((uint8_t)dec_data[off]) << std::endl;
            index--;
            cmpbuf.push_back(dec_data[off]);
            off++;
        } else {
            uint32_t lz_off = off - std::distance(dec_data.begin(), longest_pos) - 1;
            std::cerr << "Writing COMPRESSED: " << longest_len << " bytes from " << -(int)lz_off -1 << std::endl;
            byte |= 1 << index;
            index--;
            if(longest_len < 17) {
                uint8_t lenmsb = (uint8_t)((lz_off >> 8) + ((longest_len - 1) << 4));
                uint8_t lsb = (uint8_t)lz_off;
                cmpbuf.push_back(lenmsb);
                cmpbuf.push_back(lsb);
            } else if(longest_len < 0x111) {
                uint32_t len = longest_len - 0x11;
                uint8_t bytes[3] = {
                    (uint8_t)(len >> 4),
                    (uint8_t)((lz_off >> 8) + ((len & 15) << 4)),
                    (uint8_t)lz_off
                };
                cmpbuf.push_back(bytes[0]);
                cmpbuf.push_back(bytes[1]);
                cmpbuf.push_back(bytes[2]);
            } else {
                uint32_t len = longest_len - 0x111;
                uint8_t bytes[4] = {
                    (uint8_t)((len >> 12) + 0x10),
                    (uint8_t)(len >> 4),
                    (uint8_t)((lz_off >> 8) + ((len & 15) << 4)),
                    (uint8_t)lz_off
                };
                cmpbuf.push_back(bytes[0]);
                cmpbuf.push_back(bytes[1]);
                cmpbuf.push_back(bytes[2]);
                cmpbuf.push_back(bytes[3]);
            }
            off += longest_len;
        }

        if(index < 0) {
            raw_write(&byte, 1);
            raw_write(&(cmpbuf[0]), cmpbuf.size());
            byte = 0;
            index = 7;
            cmpbuf.clear();
        }
    }
    if(off % 8 != 7) {
        raw_write(&byte, 1);
        raw_write(&(cmpbuf[0]), cmpbuf.size());
    }
    uint8_t end_byte = 0xFF;
    raw_write(&end_byte, 1);
    modified = false;
}

uint64_t LZ11::read(void *dest, uint64_t size) {
    uint64_t len = std::min(size, dec_data.size() - pos);
    memmove((char*)dest, &dec_data[pos], len);
    pos += len;
    return len;
}
uint64_t LZ11::write(const void* dest, uint64_t size) {
    modified = true;
    if(pos + size < dec_data.size()) {
        memmove(&dec_data[pos], (const char*)dest, size);
        pos += size;
        return size;
    } else if(pos == dec_data.size()) {
        dec_data.insert(dec_data.end(), (const char*)dest, (const char*)dest + size);
        pos += size;
        return size;
    }
    uint64_t len1 = write(dest, std::min(size, dec_data.size() - pos));
    return len1 + write(dest, size - len1);
}
uint64_t LZ11::tell() {
    return pos;
}
void LZ11::seek(int64_t pos, std::ios_base::seekdir dir) {
    if(dir == std::ios_base::beg) {
        this->pos = pos;
    } else if(dir == std::ios_base::end) {
        this->pos = dec_data.size() - pos;
    } else {
        this->pos += pos;
    }
}

shared_ptr<LZ11> LZ11::create(std::string fname) {
    {
        std::ofstream ofile(fname, std::ios::binary);
        char header[] = {0x11, 0, 0, 0, 0, 0, 0, 0, -1};
        ofile.write(header, sizeof(header));
    }
    return RTDL::make_shared<LZ11>(fname);
}

shared_ptr<LZ11> LZ11::create(shared_ptr<IOBase> file) {
    auto pos = file->tell();
    file->write((uint32_t)0x11, Endian::Little);
    file->write((uint32_t)0, Endian::Little);
    file->write((uint8_t)0xFF);
    file->seek(pos);
    return RTDL::make_shared<LZ11>(file);
}
}
