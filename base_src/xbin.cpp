#include <rtdl/xbin.hpp>
#include <stdexcept>

namespace RTDL {
namespace {
struct RAW_header {
    uint32_t magic;
    uint16_t endian;
    uint16_t version;
    uint32_t size;
    uint32_t type;
}__attribute__((packed));
static_assert(sizeof(RAW_header) == 0x10, "the base header has to be 0x10 bytes large!");
}

XBIN::XBIN(std::string fname): IOBase(fname) {
    RAW_header header;
    _file->read((char*)&header, sizeof(header));
    if(convert<Endian::Big>((uint32_t)0x5842494e) != header.magic) // "XBIN"
        throw std::runtime_error("Invalid magic for xbin");
    if(header.endian == 0x1234)
        endian = Endian::Host;
    else
        if constexpr(Endian::Host == Endian::Little)
            endian = Endian::Big;
        else
            endian = Endian::Little;
    switch(convert<Endian::Little>(header.version)) {
        case 2:
            ver = XBINVersion::VER_2;
            break;
        case 4: {
                ver = XBINVersion::VER_4;
                uint32_t uid;
                _file->read((char*)&uid, sizeof(uid));
                this->uid = convert(uid, endian);
                break;
            }
        default:
            throw std::runtime_error("Unknown version of xbin");
    }
    size = convert(header.size, endian);
    type = convert(header.type, endian);
}

XBIN::XBIN(shared_ptr<IOBase> file): IOBase(file) {
    uint32_t magic, raw_size, raw_type, raw_uid;
    uint16_t endian_val, version_val;
    magic = _io->read<uint32_t>();
    endian_val = _io->read<uint16_t>();
    version_val = _io->read<uint16_t>();
    raw_size = _io->read<uint32_t>();
    raw_type = _io->read<uint32_t>();
    if(convert<Endian::Big>((uint32_t)0x5842494e) != magic) // "XBIN"
        throw std::runtime_error("Invalid magic for xbin");
    if(endian_val == 0x1234)
        endian = Endian::Host;
    else
        if constexpr(Endian::Host == Endian::Little)
            endian = Endian::Big;
        else
            endian = Endian::Little;
    switch(convert(version_val, endian)) {
        case 2:
            ver = XBINVersion::VER_2;
            break;
        case 4:
            ver = XBINVersion::VER_4;
            raw_uid = _io->read<uint32_t>();
            uid = convert(raw_uid, endian);
            break;
        default:
            throw std::runtime_error("Unknown version of xbin");
    }
    size = convert(raw_size, endian);
    type = convert(raw_type, endian);
}

uint64_t XBIN::read(void *dest, uint64_t size) {
    return raw_read(dest, size);
}

uint64_t XBIN::write(const void *src, uint64_t size) {
    auto retval = raw_write(src, size);
    auto pos = raw_tell();
    if(pos > this->size) {
        this->size = pos;
        raw_seek(0x8, std::ios_base::beg);
        raw_write(&size, sizeof(size));
        raw_seek(pos, std::ios_base::beg);
    }
    return retval;
}
uint64_t XBIN::tell() {
    return raw_tell() - (ver == XBINVersion::VER_2?0x10:0x14);
}
uint32_t XBIN::tellOffset() {
    return (uint32_t)raw_tell();
}
void XBIN::seek(int64_t pos, std::ios_base::seekdir dir) {
    if(dir == std::ios_base::beg)
        raw_seek(pos + (ver == XBINVersion::VER_2?0x10:0x14), dir);
    else
        raw_seek(pos, dir);
}

shared_ptr<XBIN> XBIN::create(XBINVersion ver, Endian endian, uint32_t type, uint32_t uid, shared_ptr<IOBase> base) {
    auto pos = base->tell();
    base->write<uint32_t, Endian::Big>(0x5842494e);
    base->write((uint16_t)0x1234, endian);
    base->write((uint16_t)ver, endian);
    base->write((uint32_t)(ver == XBINVersion::VER_2? 0x10 : 0x14), endian);
    base->write(type, endian);
    if(ver == XBINVersion::VER_4)
        base->write(uid, endian);
    base->seek(pos);
    return RTDL::make_shared<XBIN>(base);
}

shared_ptr<XBIN> XBIN::create(XBINVersion ver, Endian endian, uint32_t type, uint32_t uid, std::string fname) {
    {
        RAW_header header;
        header.magic = convert<Endian::Big>((uint32_t)0x5842494e);
        header.endian = convert((uint16_t)0x1234, endian);
        header.version = convert((uint16_t)ver, endian);
        header.size = convert((uint32_t)(ver == XBINVersion::VER_2? 0x10 : 0x14), endian);
        header.type = convert(type, endian);
        std::ofstream f(fname, std::ios::binary);
        f.write((char*)&header, sizeof(header));
        if(ver == XBINVersion::VER_4)
            f.write((char*)&uid, sizeof(uid));
    }
    return RTDL::make_shared<XBIN>(fname);
}
}
