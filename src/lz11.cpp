#include <rtdl/lz11.hpp>
#include <iostream>
#include <cxxopts.hpp>

using namespace RTDL;
int main(int argc, char** argv) {
    try {
        cxxopts::Options options(argv[0], "LZ11 compressor/decompressor");
        bool decompress;
        options.add_options()("d", "Decompress (as opposed to compress)", cxxopts::value<bool>(decompress)->default_value("false"))
                             ("h,help", "Show this message", cxxopts::value<bool>()->default_value("false"))
                             ("i,input", "Input", cxxopts::value<std::string>())
                             ("o,output", "Output", cxxopts::value<std::string>());
        options.parse_positional({"input", "output"});
        auto result = options.parse(argc, argv);
        if(result.count("help")) {
            std::cerr << options.help({"", "Group"}) << std::endl;
            return 0;
        }

        if(!result.count("input") || !result.count("output")) {
            std::cerr << "You need to pass both an input and an output file!" << std::endl;
            std::cerr << options.help({"", "Group"}) << std::endl;
            return 1;
        }
        std::string input = result["input"].as<std::string>();
        std::string output = result["output"].as<std::string>();

        if(input == "-")
            input = "/dev/stdin";
        if(output == "-")
            output = "/dev/stdout";

        if(decompress) {
            auto decompressor = RTDL::make_shared<LZ11>(input);
            std::ofstream out(output, std::ios::binary);
            out.write(&(decompressor->dec_data[0]), decompressor->dec_data.size());
            std::cerr << "Inflated " << decompressor->_file->tellg() << " bytes to " << decompressor->dec_data.size() << std::endl;
        } else {
            std::ifstream in(input, std::ios::binary | std::ios::ate);
            auto size = in.tellg();
            char *data = new char[size];
            in.seekg(0);
            in.read(data, size);
            auto compressor = LZ11::create(output);
            compressor->write(data, size);
            compressor->compress();
            std::cerr << "Deflated " << size << " bytes to " << compressor->_file->tellg() << std::endl;
        }
    } catch (const cxxopts::OptionException& e) {
        std::cout << "error parsing options: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
