#include <rtdl/xbin.hpp>
#include <iostream>
#include <stdexcept>

int main(int argc, char **argv) {
    if(argc < 2) {
        std::cerr << "Usage: xbininfo [filename]" << std::endl;
        return 1;
    }
    try {
        RTDL::XBIN xbin(argv[1]);
        std::cout << "XBIN file " << argv[1] << ":" << std::endl;
        std::cout << "Version " << (uint16_t)xbin.ver << ", Size: " << xbin.size << std::endl;
        if(xbin.ver == RTDL::XBINVersion::VER_4)
            std::cout << "Type: " << xbin.type << ", UID: " << xbin.uid << std::endl;
        else
            std::cout << "Type: " << xbin.type << std::endl;

    } catch(std::runtime_error e) {
        std::cerr << "Runtime error detected: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
