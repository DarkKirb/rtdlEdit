#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <rtdl/endian.hpp>
#include <rtdl/ptr.hpp>
#include <rtdl/xbin.hpp>
using namespace RTDL;

struct test_int_struct {
    uint32_t data;
}__attribute__((packed));

TEST_CASE("Endian Test", "[endian]") {
    REQUIRE(bswap((uint16_t)0x0123) == 0x2301);
    REQUIRE(bswap((uint32_t)0x01234567) == 0x67452301);
    REQUIRE(bswap((uint64_t)0x0123456789abcdefull) == 0xefcdab8967452301ull);
    REQUIRE(convert<Endian::Little, Endian::Little>((uint16_t)0x1234) == 0x1234);
    REQUIRE(convert<Endian::Big, Endian::Little>((uint16_t)0x1234) == 0x3412);
    REQUIRE(convert<test_int_struct>({0x12345678}, Endian::Big, Endian::Little).data == 0x78563412);
}

TEST_CASE("Managed PTR test", "[ptr]") {
    auto int_struct = make_shared<test_int_struct>();
    int_struct->data = 0x12345678;
    REQUIRE((bool) int_struct);
    REQUIRE(int_struct->data == 0x12345678);
    auto copy = int_struct;
    REQUIRE(int_struct.info == copy.info);
    copy = make_shared<test_int_struct>(&*int_struct);
    REQUIRE(int_struct.info == copy.info);
    auto bad_ptr = make_shared<int>((int*)nullptr);
    REQUIRE(!(bool)bad_ptr);
    REQUIRE_THROWS(*bad_ptr);
    weak_ptr<int> bad_weak_ptr((int*)nullptr);
    REQUIRE_THROWS(*bad_weak_ptr);
    REQUIRE(!(bool)bad_weak_ptr.lock());
}

TEST_CASE("XBIN test", "[xbin]") {
    REQUIRE_THROWS(make_shared<XBIN>("."));
    auto xbin = XBIN::create(XBINVersion::VER_4, Endian::Little, 0x1234, 0x23456, "test.xbin");
    REQUIRE(xbin->ver == XBINVersion::VER_4);
    REQUIRE(xbin->endian == Endian::Little);
    REQUIRE(xbin->type == 0x1234);
    REQUIRE(xbin->uid == 0x23456);

    auto subxbin = XBIN::create(XBINVersion::VER_2, Endian::Big, 0x4321, 0, xbin);
    REQUIRE(xbin->size == 0x24);
    REQUIRE(subxbin->size == 0x10);
    REQUIRE(subxbin->ver == XBINVersion::VER_2);
    REQUIRE(subxbin->endian == Endian::Big);
    REQUIRE(subxbin->type == 0x4321);
}
