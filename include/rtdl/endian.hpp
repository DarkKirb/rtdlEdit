#pragma once
#include <stdint.h>

namespace RTDL {

/**
 * This enum contains all supported endians
 */
enum class Endian {
    Little,
    Big,
    Wii = Big, ///< Endianness of a Wii
    N3DS = Little, ///< Endianness of a 3DS
    Switch = Little, ///< Endianness of a Switch
    Host = (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)? Little: Big ///< Endianness of the current system
};

/**
 * Swaps the bytes in `val`, for example 0x1122 -> 0x2211
 */
constexpr uint16_t bswap(uint16_t val) {
    return (uint16_t)((val >> 8) + ((val & 0xFF) << 8));
}
/**
 * Swaps the bytes in `val`, for example 0x11223344 -> 0x44332211
 */
constexpr uint32_t bswap(uint32_t val) {
    return (uint32_t)(bswap((uint16_t)(val >> 16)) + ((uint32_t)bswap((uint16_t)val) << 16));
}
/**
 * Swaps the bytes in `val`, for example 0x1122334455667788 -> 0x8877665544332211
 */
constexpr uint64_t bswap(uint64_t val) {
    return (uint64_t)(bswap((uint32_t)(val >> 32)) + ((uint64_t)bswap((uint32_t)val) << 32));
}

/**
 * Statically Converts a number from one endian to another
 */
template<Endian from, Endian to=Endian::Host>
constexpr uint16_t convert(uint16_t val) {
    if constexpr(from != to)
        return bswap(val);
    return val;
}

/**
 * Converts a number from one endian to another
 */
template<Endian from, Endian to=Endian::Host>
constexpr uint32_t convert(uint32_t val) {
    if constexpr(from != to)
        return bswap(val);
    return val;
}

/**
 * Converts a number from one endian to another
 */
template<Endian from, Endian to=Endian::Host>
constexpr uint64_t convert(uint64_t val) {
    if constexpr(from != to)
        return bswap(val);
    return val;
}

/**
 * Converts a number from one endian to another
 */
template<typename T>
T convert(T val, Endian from, Endian to=Endian::Host) {
    if(from == to)
        return val;
    T out;
    char *src = (char*)&val;
    char *dest = (char*)&out + sizeof(T);
    for(int i = 0; i < sizeof(T); i++)
        *(--dest) = *(src++);
    return out;
}
template<>
uint16_t convert<uint16_t>(uint16_t val, Endian from, Endian to);
template<>
uint32_t convert<uint32_t>(uint32_t val, Endian from, Endian to);
template<>
uint64_t convert<uint64_t>(uint64_t val, Endian from, Endian to);
}
