#pragma once
#include <string>
#include <fstream>
#include <stdint.h>
#include <stdexcept>
#include "ptr.hpp"
#include "endian.hpp"

namespace RTDL {

/**
 * This structure is the base for all IO classes used in RTDLedit
 */
struct IOBase {
    std::fstream *_file; ///< File that is being read from
    shared_ptr<IOBase> _io; ///< Chained IO instance
    uint64_t _io_pos; ///< Current Chained IO position
    uint64_t offset; ///< Offset from start of file
    uint64_t size; ///< Size of file

    IOBase(std::string fname); ///< Constructs a file-based IOBase
    IOBase(shared_ptr<IOBase> file); ///< Constructs a IOBase-based IOBase
    IOBase(shared_ptr<IOBase> file, uint64_t size); ///< Constructs a IOBase-based IOBase with a specific maximum size
    virtual ~IOBase(); ///< Destructs an IOBase, freeing any used resources
    IOBase(const IOBase &o) = delete;
    IOBase &operator=(const IOBase &o) = delete;

    virtual uint64_t read(void *dest, uint64_t size) = 0; ///< Reads from the IO base. Returns the number of bytes read
    virtual uint64_t write(const void *src, uint64_t size) = 0; ///< Writes to the IO base. Returns the number of bytes written
    virtual uint64_t tell() = 0; ///< Returns the current position in the stream
    virtual void seek(int64_t pos, std::ios_base::seekdir dir=std::ios_base::beg) = 0; ///< Seeks through the stream

    /**
     * Reads an object from the IO base. This will not change endianness whatsoever. Throws an std::runtime_error if the stream ends before the object is read
     */
    template<typename T>
    T read() {
        T obj;
        if(read(&obj, sizeof(obj)) != sizeof(obj))
            throw std::runtime_error("Couldn't read enough data from IO");
        return obj;
    }

    /**
     * Reads an object from the IO base and swaps endian if necessary. Throws an std::runtime_error if the stream ends before the object is read
     */
    template<typename T, Endian src>
    T read() {
        if constexpr(src == Endian::Host)
            return read<T>();
        return convert<T>(read<T>(), src);
    }
    /**
     * Reads an object from the IO base and swaps endian if necessary. Throws an std::runtime_error if the stream ends before the object is read
     */
    template<typename T>
    T read(Endian src) {
        if(src == Endian::Host)
            return read<T>();
        return convert<T>(read<T>(), src);
    }

    /**
     * Writes an object to the IO base. This will not change endianness whatsoever.
     */
    template<typename T>
    void write(T obj) {
        write(&obj, sizeof(obj));
    }

    /**
     * Writes an object to the IO base and swaps endian if necessary.
     */
    template<typename T, Endian src>
    void write(T obj) {
        if constexpr(src == Endian::Host)
            return write<T>(obj);
        T swapped = convert<T>(obj, src);
        write<T>(swapped);
    }

    /**
     * Writes an object to the IO base and swaps endian if necessary.
     */
    template<typename T>
    void write(T obj, Endian src) {
        if(src == Endian::Host)
            return write<T>(obj);
        T swapped = convert<T>(obj, src);
        write<T>(swapped);
    }

    protected:
    uint64_t raw_read(void *dest, uint64_t size); ///< Reads from the IO base. Returns the number of bytes read
    uint64_t raw_write(const void *src, uint64_t size); ///< Writes to the IO base. Returns the number of bytes written
    uint64_t raw_tell(); ///< Returns the current position in the stream
    void raw_seek(int64_t pos, std::ios_base::seekdir dir); ///< Seeks through the stream
};

}
