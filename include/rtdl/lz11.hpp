#pragma once
#include "io.hpp"
#include <vector>

namespace RTDL {

/**
 * This class enables reading and writing LZ11-compressed files.
 */
struct LZ11: IOBase {
    std::vector<char> dec_data; ///< Contains the decompressed data
    uint64_t pos;
    bool modified;

    LZ11(std::string fname); ///< Constructs a file-based LZ11
    LZ11(shared_ptr<IOBase> file); ///< Constructs a IOBase-based LZ11
    virtual ~LZ11(); ///< Saves the file to disk

    void decompress(); ///< Reads the file from disk. This is done by the constructor already.
    void compress(); ///< Writes the file to disk. This is done by the destructor already

    virtual uint64_t read(void *dest, uint64_t size); ///< Reads data from the file
    virtual uint64_t write(const void *src, uint64_t size); ///< Writes to the file
    virtual uint64_t tell(); ///< Returns the current position in file
    virtual void seek(int64_t pos, std::ios_base::seekdir dir=std::ios_base::beg); ///< Seeks through the stream

    static shared_ptr<LZ11> create(std::string fname); ///< Creates an empty LZ11 file
    static shared_ptr<LZ11> create(shared_ptr<IOBase> file); ///< Creates an empty LZ11 file
};

}
