#pragma once
#include "io.hpp"

namespace RTDL {
/**
 * This enum contains the two versions of XBINs
 */
enum class XBINVersion: uint16_t {
    VER_2 = 2,
    VER_4 = 4
};
/**
 * This class is for XBIN files, the main binary format used in RTDL engine games
 */
struct XBIN: IOBase {
    XBINVersion ver; ///< Version of the current XBIN file
    Endian endian; ///< Endian of the XBIN file
    uint32_t uid; ///< UID of the XBIN file. Only for XBINVersion::VER_4
    uint32_t type; ///< Type of the XBIN file. TODO: find out possible values
    XBIN(std::string fname); ///< Open a XBIN file from disk
    XBIN(shared_ptr<IOBase> file); ///< Open a XBIN file from an IOBase

    virtual uint64_t read(void *dest, uint64_t size); ///< Reads from the XBIN
    virtual uint64_t write(const void *src, uint64_t size); ///< Writes to the XBIN
    virtual uint64_t tell(); ///< Returns the offset from the XBIN data begin
    virtual uint32_t tellOffset(); ///< Returns the offset from the File begin
    virtual void seek(int64_t pos, std::ios_base::seekdir dir=std::ios_base::beg); ///< Seeks through the XBIN

    static shared_ptr<XBIN> create(XBINVersion ver, Endian endian, uint32_t type, uint32_t uid, shared_ptr<IOBase> base); ///< creates a new XBIN in an IOBase
    static shared_ptr<XBIN> create(XBINVersion ver, Endian endian, uint32_t type, uint32_t uid, std::string fname); ///< creates a new XBIN in a file
};
}
