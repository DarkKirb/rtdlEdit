/*
 * This file contains a managed pointer implementation
 *
 * The reason why this file exists is because I had problems getting `std::shared_ptr` and `std::weak_shared_ptr` to work with classes
 * This implementation supports making a `shared_ptr` from `this` without having two independent `shared_ptr`s or without having to subclass `std::enable_shared_from_this<T>`
 *
 * This code is released under public domain
 */
#pragma once
#include <stdexcept>
#include <unordered_map>

namespace RTDL {
/**
 * Class that contains information about a shared or weak pointer
 *
 */
template<typename T>
struct ptrinfo {
    int shared_use_count; ///< Number of `shared_ptr`s using this pointer
    int weak_use_count; ///< Number of `weak_ptr`s using this pointer
    bool valid; ///< If true, ptr points to a valid location (not `delete`d, not `nullptr`)
    T *ptr; ///< Pointer to the object
    ptrinfo(): shared_use_count(0), weak_use_count(0), valid(false), ptr(nullptr) {}
};

/**
 * This namespace is used internally
 */
namespace ptr {
template<typename T>
std::unordered_map<T*, ptrinfo<T>*> &get_map() {
    static std::unordered_map<T*, ptrinfo<T>*> map;
    return map;
}
}

template<typename T>
struct weak_ptr;

/**
 * This class is like `std::shared_pointer<T>`
 */
template<typename T>
struct shared_ptr {
    ptrinfo<T> *info;
    /**
     * Constructs a new shared pointer from a raw pointer
     */
    shared_ptr(T* obj) {
        info = new ptrinfo<T>;
        info->shared_use_count = 1;
        info->weak_use_count = 0;
        info->valid = obj != nullptr;
        info->ptr = obj;
        ptr::get_map<T>()[obj] = info;
    }
    /**
     * Copies a shared pointer
     */
    shared_ptr(const shared_ptr<T> &sptr) {
        info = (ptrinfo<T>*)sptr.info;
        info->shared_use_count++;
    }
    /**
     * Locks a weak pointer (= converts to a shared pointer)
     */
    shared_ptr(weak_ptr<T> &wptr) {
        info = (ptrinfo<T>*)wptr.info;
        info->shared_use_count++;
    }
    /**
     * Creates a shared pointer from a `ptrinfo<T> *`
     */
    shared_ptr(ptrinfo<T> *ptr) {
        info = ptr;
        info->shared_use_count++;
    }
    /**
     * Deallocates a shared pointer. This will `delete info->ptr;` and `delete info;` depending on the two refcounts.
     */
    ~shared_ptr() {
        if(info->shared_use_count == 1) {
            if(info->valid) {
                ptr::get_map<T>().erase(info->ptr);
                delete info->ptr;
            }
            info->shared_use_count--;
            info->valid = false;
            if(info->weak_use_count == 0) {
                delete info;
            }
        }
    }
    /**
     * This dereferences a shared pointer. It throws a `std::runtime_error` if an invalid shared pointer is dereferenced
     */
    T &operator *() {
        if(info->valid)
            return *(info->ptr);
        throw std::runtime_error("Bad shared pointer");
    }
    /**
     * This dereferences a shared pointer. It throws a `std::runtime_error` if an invalid shared pointer is dereferenced
     */
    T *operator->() {
        if(info->valid)
            return info->ptr;
        throw std::runtime_error("Bad shared pointer");
    }
    /**
     * This returns true if the shared pointer is valid
     */
    operator bool() const {
        return info->valid;
    }
    /**
     * this compares two shared pointers for equality
     */
    bool operator==(const shared_ptr<T> &o) const {
        return info->ptr == o.info->ptr;
    }
    /**
     * this creates a weak pointer from this shared pointer
     */
    operator weak_ptr<T>() {
        return weak_ptr<T>(*this);
    }
    /**
     * This casts this shared pointer to `shared_ptr<T2>`, but only if `T` is derived from `T2`
     */
    template<typename T2>
    operator shared_ptr<T2>() {
        static_assert(std::is_base_of<T2, T>::value, "T not derived from T2");
        return *reinterpret_cast<shared_ptr<T2> *>(this);
    }
    /**
     * This casts this shared pointer to `weak_ptr<T2>`, but only if `T` is derived from `T2`
     */
    template<typename T2>
    operator weak_ptr<T2>() {
        return weak_ptr<T2>((shared_ptr<T2>)*this);
    }
};

/**
 * This class is like `std::weak_ptr<T>`
 */
template<typename T>
struct weak_ptr {
    ptrinfo<T> *info;
    /**
     * Creates a weak pointer from a shared pointer
     */
    weak_ptr(const shared_ptr<T> &sptr) {
        info = (ptrinfo<T>*)sptr.info;
        info->weak_use_count++;
    }
    /**
     * Copies a weak pointer
     */
    weak_ptr(const weak_ptr<T> &wptr) {
        info = (ptrinfo<T>*)wptr.info;
        info->weak_use_count++;
    }
    /**
     * Deletes a weak pointer. may `delete info;` if both use counts are 0
     */
    ~weak_ptr() {
        info->weak_use_count--;
        if(info->shared_use_count == 0 && info->weak_use_count == 0)
            delete info;
    }

    /**
     * converts the weak pointer into a shared pointer. Throws an `std::runtime_error` if the pointer is invalid
     */
    shared_ptr<T> operator *() {
        if(info->valid)
            return shared_ptr<T>(*this);
        throw std::runtime_error("Bad weak pointer");
    }
    /**
     * converts the weak pointer into a shared pointer. No exceptions are thrown if the pointer is invalid
     */
    shared_ptr<T> lock() {
        return shared_ptr<T>(*this);
    }
};

/**
 * Constructs a new shared pointer using the argument list
 */
template<typename T, typename... Args>
shared_ptr<T> make_shared(Args... args) {
    shared_ptr<T> p(new T(args...));
    return p;
}

/**
 * Creates a new shared pointer from a pointer to a dynamically(!) allocated object that is either not managed at all or managed by another `shared_ptr<T>`
 */
template<typename T>
shared_ptr<T> make_shared(T* ptr) {
    auto &map = ptr::get_map<T>();
    if(map.count(ptr)) {
        return shared_ptr<T>(map[ptr]);
    }
    return shared_ptr<T>(ptr);
}

}

